package com.example.anotaai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class EnterSuccessActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_success);
    }

    public void toDashboard(View view) {
        /* Get user_id from previous activity */
        Bundle extras = getIntent().getExtras();
        long user_id = extras.getLong("user_id");

        Intent i = new Intent(EnterSuccessActivity.this, DashboardActivity.class);

        /* Set user_id for the next activity */
        i.putExtra("user_id", user_id);

        startActivity(i);
    }
}
