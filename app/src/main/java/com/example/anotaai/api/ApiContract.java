package com.example.anotaai.api;

public class ApiContract {
    /* Prevent accidental instantiation */
    private ApiContract() {}

    /* API Constants */
    public static final String API_URL = "https://3859-2804-187c-8120-c0e0-c55e-9545-6352-e467.ngrok.io/api";
}
