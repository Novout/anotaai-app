package com.example.anotaai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anotaai.api.ApiContract;
import com.example.anotaai.models.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        /* Get user_id from previous activity */
        Bundle extras = getIntent().getExtras();
        long user_id = extras.getLong("user_id");

        /* Fetch tasks */
        String route = String.format("/tasks?user_id=%d", user_id);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, ApiContract.API_URL + route, null,
                new Response.Listener<JSONObject> () {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());

                        try {
                            JSONArray data = response.getJSONArray("data");

                            List<Task> tasks = new ArrayList<>();

                            for(int i = 0; i < data.length(); i++) {
                                JSONObject taskJSON = data.getJSONObject(i);

                                long id = taskJSON.getLong("id");
                                long user_id = taskJSON.getLong("user_id");
                                String title = taskJSON.getString("title");
                                String description = taskJSON.getString("description");
                                boolean done = taskJSON.getBoolean("done");
                                Date due = new Date(taskJSON.getString("due"));

                                tasks.add(new Task(id, user_id, title, description, done, due));
                            }

                            System.out.println(tasks.toString());

                            Toast.makeText(getApplicationContext(), String.format("Você tem %d tarefas!", tasks.size()), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void onCreate(View view) {
        /* Get user_id from previous activity */
        Bundle extras = getIntent().getExtras();
        long user_id = extras.getLong("user_id");

        Intent i = new Intent(DashboardActivity.this, DashboardCreateActivity.class);

        /* Set user_id for the next activity */
        i.putExtra("user_id", user_id);

        startActivity(i);
    }
}
