package com.example.anotaai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.anotaai.api.ApiContract;
import com.example.anotaai.dao.UserDAO;
import com.example.anotaai.models.User;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    UserDAO user;

    EditText email;
    EditText password;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = new UserDAO(this.getApplicationContext());

        email = (EditText)findViewById(R.id.form_email);
        password = (EditText)findViewById(R.id.form_password);
        button = (Button)findViewById(R.id.form_enter);

    }

    public void onEnter(View view) {
        String _email = email.getText().toString();
        String _password = password.getText().toString();

        if(!_email.isEmpty() && !_password.isEmpty()) {
//            User _user = new User(_email, _password);
//
//            if(user.validate(_user)) {
//                return;
//            }
//
//            user.insert(_user);
//
//            Intent i = new Intent(MainActivity.this, EnterSuccessActivity.class);
//
//            startActivity(i);
//
//            i.putExtra("email", _user.getEmail());
            validateUser(_email, _password);
        }
    }

    private void validateUser(String email, String password) {
        String data = String.format("{\"email\": \"%s\", \"password\": \"%s\"}", email, password);

        JSONObject dataJSON = new JSONObject();

        try {
            dataJSON = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ApiContract.API_URL + "/auth/login", dataJSON,
                new Response.Listener<JSONObject> () {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());

                        try {
                            JSONObject data = response.getJSONObject("data");
                            long id = data.getLong("id");
                            String email = data.getString("email");
                            String password = data.getString("password");

                            User user = new User(id, email, password);

                            Intent i = new Intent(MainActivity.this, EnterSuccessActivity.class);

                            /* Set user_id for the next activity */
                            i.putExtra("user_id", user.getId());

                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}