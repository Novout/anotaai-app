package com.example.anotaai;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.anotaai.dao.TaskDAO;
import com.example.anotaai.models.Task;

import java.util.Date;
import java.util.Locale;

public class DashboardCreateActivity extends Activity {
    TaskDAO task;

    EditText title;
    EditText description;

    Button timerPicker;
    Button create;

    int hour;
    int minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_create);

        title = findViewById(R.id.dashboard_create_title);
        description = findViewById(R.id.dashboard_create_description);

        timerPicker = findViewById(R.id.dashboard_create_timer);
        create = findViewById(R.id.dashboard_create_button);

        task = new TaskDAO(this.getApplicationContext());
    }

    public void timePicker(View view) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour) {
                hour = hourOfDay;
                minute = minuteOfHour;

                timerPicker.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minute));
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, true);

        timePickerDialog.setTitle(R.string.dashboard_create_timer_title);
        timePickerDialog.show();
    }

    public void onClose(View view) {
        /* Get user_id from previous activity */
        Bundle extras = getIntent().getExtras();
        long user_id = extras.getLong("user_id");

        Intent i = new Intent(DashboardCreateActivity.this, DashboardActivity.class);

        /* Set user_id for the next activity */
        i.putExtra("user_id", user_id);

        startActivity(i);
    }

    public void onCreateEntity(View view) {
//        task.insert(new Task(0, title.getText().toString(), description.getText().toString(), false, new Date()));

        onClose(view);
    }
}
