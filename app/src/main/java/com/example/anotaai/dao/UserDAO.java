package com.example.anotaai.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.anotaai.database.DatabaseContract;
import com.example.anotaai.database.DatabaseHelper;
import com.example.anotaai.models.User;

import java.util.ArrayList;

public class UserDAO {
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;
    private Context context;

    public UserDAO(Context context) {
        this.context = context;

        this.databaseHelper = new DatabaseHelper(this.context);
    }

    public DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    public void open() {
        this.database = this.databaseHelper.getWritableDatabase();
    }

    public void close() {
        this.database.close();
        this.databaseHelper.close();
    }

    public void insert(User user) {
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users.COLUMN_NAME_EMAIL, user.getEmail());
        values.put(DatabaseContract.Users.COLUMN_NAME_PASSWORD, user.getPassword());

        long id = database.insert(DatabaseContract.Users.TABLE_NAME, null, values);

        user.setId(id);
    }

    public User get(long id) {
        String[] columns = {
                DatabaseContract.Users.COLUMN_NAME_ID,
                DatabaseContract.Users.COLUMN_NAME_EMAIL,
                DatabaseContract.Users.COLUMN_NAME_PASSWORD
        };

        String selection = DatabaseContract.Users.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        Cursor cursor = database.query(DatabaseContract.Users.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if(cursor.getCount() == 0) {
            cursor.close();
            return null;
        }

        cursor.moveToFirst();

        User user = new User(
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getString(2)
        );

        cursor.close();

        return user;
    }

    public ArrayList<User> getAll() {
        String[] columns = {
                DatabaseContract.Users.COLUMN_NAME_ID,
                DatabaseContract.Users.COLUMN_NAME_EMAIL,
                DatabaseContract.Users.COLUMN_NAME_PASSWORD
        };

        Cursor cursor = database.query(DatabaseContract.Users.TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null);

        ArrayList<User> users = new ArrayList<>();

        while(cursor.moveToNext())
            users.add(new User(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2)
            ));

        cursor.close();

        return users;
    }

    public int update(long id, User user) {
        String selection = DatabaseContract.Users.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Users.COLUMN_NAME_EMAIL, user.getEmail());
        values.put(DatabaseContract.Users.COLUMN_NAME_PASSWORD, user.getPassword());

        return database.update(DatabaseContract.Users.TABLE_NAME, values, selection, selectionArgs);
    }

    public int delete(long id) {
        String selection = DatabaseContract.Users.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        return database.delete(DatabaseContract.Users.TABLE_NAME, selection, selectionArgs);
    }

    public boolean validate(User user) {
        String[] columns = {
                DatabaseContract.Users.COLUMN_NAME_ID
        };

        String selection = DatabaseContract.Users.COLUMN_NAME_EMAIL + " = ? AND " +
                DatabaseContract.Users.COLUMN_NAME_PASSWORD + " = ?";
        String[] selectionArgs = {
                user.getEmail(),
                user.getPassword()
        };

        Cursor cursor = database.query(DatabaseContract.Users.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if(cursor.getCount() == 0) {
            cursor.close();
            return false;
        }

        cursor.close();

        return true;
    }
}
