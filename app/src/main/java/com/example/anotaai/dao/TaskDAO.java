package com.example.anotaai.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.anotaai.database.DatabaseContract;
import com.example.anotaai.database.DatabaseHelper;
import com.example.anotaai.models.Task;

import java.util.ArrayList;
import java.util.Date;

public class TaskDAO {
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;
    private Context context;

    public TaskDAO(Context context) {
        this.context = context;

        this.databaseHelper = new DatabaseHelper(this.context);
    }

    public DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    public void open() {
        this.database = this.databaseHelper.getWritableDatabase();
    }

    public void close() {
        this.database.close();
        this.databaseHelper.close();
    }

    public void insert(Task task) {
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Tasks.COLUMN_NAME_USER_ID, task.getUserId());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_TITLE, task.getTitle());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_DESCRIPTION, task.getDescription());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_DONE, task.isDone());

        if(task.getDue() != null)
            values.put(DatabaseContract.Tasks.COLUMN_NAME_DUE, task.getDue().getTime());

        long id = database.insert(DatabaseContract.Tasks.TABLE_NAME, null, values);

        task.setId(id);
    }

    public Task get(long id) {
        String[] columns = {
                DatabaseContract.Tasks.COLUMN_NAME_ID,
                DatabaseContract.Tasks.COLUMN_NAME_USER_ID,
                DatabaseContract.Tasks.COLUMN_NAME_TITLE,
                DatabaseContract.Tasks.COLUMN_NAME_DESCRIPTION,
                DatabaseContract.Tasks.COLUMN_NAME_DONE,
                DatabaseContract.Tasks.COLUMN_NAME_DUE
        };

        String selection = DatabaseContract.Tasks.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        Cursor cursor = database.query(DatabaseContract.Tasks.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if(cursor.getCount() == 0) {
            cursor.close();
            return null;
        }

        cursor.moveToFirst();

        Task task = new Task(
                cursor.getLong(0),
                cursor.getLong(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getInt(4) == 1,
                new Date(cursor.getLong(5))
        );

        cursor.close();

        return task;
    }

    public ArrayList<Task> getAll() {
        String[] columns = {
                DatabaseContract.Tasks.COLUMN_NAME_ID,
                DatabaseContract.Tasks.COLUMN_NAME_USER_ID,
                DatabaseContract.Tasks.COLUMN_NAME_TITLE,
                DatabaseContract.Tasks.COLUMN_NAME_DESCRIPTION,
                DatabaseContract.Tasks.COLUMN_NAME_DONE,
                DatabaseContract.Tasks.COLUMN_NAME_DUE
        };

        Cursor cursor = database.query(DatabaseContract.Tasks.TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null);

        ArrayList<Task> tasks = new ArrayList<>();

        while(cursor.moveToNext())
            tasks.add(new Task(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3) == 1,
                    new Date(cursor.getLong(4))
            ));

        cursor.close();

        return tasks;
    }

    public ArrayList<Task> getAll(long userId) {
        String[] columns = {
                DatabaseContract.Tasks.COLUMN_NAME_ID,
                DatabaseContract.Tasks.COLUMN_NAME_USER_ID,
                DatabaseContract.Tasks.COLUMN_NAME_TITLE,
                DatabaseContract.Tasks.COLUMN_NAME_DESCRIPTION,
                DatabaseContract.Tasks.COLUMN_NAME_DONE,
                DatabaseContract.Tasks.COLUMN_NAME_DUE
        };

        String selection = DatabaseContract.Tasks.COLUMN_NAME_USER_ID + " = ?";
        String[] selectionArgs = {Long.toString(userId)};

        Cursor cursor = database.query(DatabaseContract.Tasks.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        ArrayList<Task> tasks = new ArrayList<>();

        while(cursor.moveToNext())
            tasks.add(new Task(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3) == 1,
                    new Date(cursor.getLong(4))
            ));

        cursor.close();

        return tasks;
    }

    public int update(long id, Task task) {
        String selection = DatabaseContract.Tasks.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Tasks.COLUMN_NAME_USER_ID, task.getUserId());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_TITLE, task.getTitle());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_DESCRIPTION, task.getDescription());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_DONE, task.isDone());
        values.put(DatabaseContract.Tasks.COLUMN_NAME_DUE, task.getDue().getTime());

        return database.update(DatabaseContract.Tasks.TABLE_NAME, values, selection, selectionArgs);
    }

    public int delete(long id) {
        String selection = DatabaseContract.Tasks.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {Long.toString(id)};

        return database.delete(DatabaseContract.Tasks.TABLE_NAME, selection, selectionArgs);
    }
}
