package com.example.anotaai.models;

import java.util.Date;

public class Task {
    private long id;
    private long userId;
    private String title;
    private String description;
    private boolean done;
    private Date due;

    public Task(long id, long userId, String title, String description, boolean done, Date due) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.done = done;
        this.due = due;
    }

    public Task(long userId, String title, String description, boolean done, Date due) {
        this.id = 0;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.done = done;
        this.due = due;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Date getDue() {
        return due;
    }

    public void setDue(Date due) {
        this.due = due;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", done=" + done +
                ", due=" + due +
                '}';
    }
}


