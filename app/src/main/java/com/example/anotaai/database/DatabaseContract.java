package com.example.anotaai.database;

import android.provider.BaseColumns;

/**
 * Database Contract
 */
public final class DatabaseContract {
    /* Prevent accidental instantiation */
    private DatabaseContract() {}

    /* Database Constants */
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "AnotaAi.db";

    /* SQL Create Database */
    public static final String SQL_CREATE_DATABASE =
            Users.SQL_CREATE_TABLE +
            Tasks.SQL_CREATE_TABLE;

    /* SQL Drop Database */
    public static final String SQL_DROP_DATABASE =
            Users.SQL_DROP_TABLE +
            Tasks.SQL_DROP_TABLE;

    /* Define the 'users' table contents */
    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "users";

        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_PASSWORD = "password";

        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAME_EMAIL + " TEXT UNIQUE NOT NULL," +
                    COLUMN_NAME_PASSWORD + " TEXT NOT NULL" +
                ");";

        public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

    /* Define the 'tasks' table contents */
    public static class Tasks implements BaseColumns {
        public static final String TABLE_NAME = "tasks";

        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_DONE = "done";
        public static final String COLUMN_NAME_DUE = "due";

        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_USER_ID + " INTEGER NOT NULL," +
                        COLUMN_NAME_TITLE + " TEXT NOT NULL," +
                        COLUMN_NAME_DESCRIPTION + " TEXT," +
                        COLUMN_NAME_DONE + " BOOLEAN DEFAULT FALSE," +
                        COLUMN_NAME_DUE + " DATETIME," +
                        "FOREIGN KEY (" + COLUMN_NAME_USER_ID + ")" +
                            "REFERENCES " + Users.TABLE_NAME + " (" + Users.COLUMN_NAME_ID + ")" +
                                "ON DELETE CASCADE ON UPDATE CASCADE" +
                ");";

        public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }
}
