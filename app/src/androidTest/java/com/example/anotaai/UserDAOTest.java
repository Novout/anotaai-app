package com.example.anotaai;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import android.content.Context;

import com.example.anotaai.dao.UserDAO;
import com.example.anotaai.database.DatabaseContract;
import com.example.anotaai.models.User;

import java.util.ArrayList;

@RunWith(AndroidJUnit4.class)
public class UserDAOTest {
    @Test
    public void insert() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        assertTrue(user.getId() > 0);

        userDAO.close();
    }

    @Test
    public void get() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        User result = userDAO.get(user.getId());

        assertNotNull(result);
        assertSame(result.getId(), user.getId());
        assertEquals(result.getEmail(), user.getEmail());
        assertEquals(result.getPassword(), user.getPassword());

        userDAO.close();
    }

    @Test
    public void getAll() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");
        User otherUser = new User("jane.doe@domain.com", "superSecret");

        userDAO.insert(user);
        userDAO.insert(otherUser);

        ArrayList<User> users = userDAO.getAll();

        assertEquals(users.size(), 2);

        userDAO.close();
    }

    @Test
    public void update() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        user.setEmail("jane.doe@domain.com");
        user.setPassword("superSecret");

        assertTrue(userDAO.update(user.getId(), user) > 0);

        User result = userDAO.get(user.getId());

        assertNotNull(result);
        assertSame(result.getId(), user.getId());
        assertEquals(result.getEmail(), user.getEmail());
        assertEquals(result.getPassword(), user.getPassword());

        userDAO.close();
    }

    @Test
    public void delete() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        assertEquals(userDAO.delete(user.getId()), 1);

        userDAO.close();
    }

    @Test
    public void validate() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");
        User wrongEmailUser = new User("jane.doe@domain.com", "secret");
        User wrongPasswordUser = new User("jhon.doe@domain.com", "superSecret");

        userDAO.insert(user);

        assertTrue(userDAO.validate(user));
        assertFalse(userDAO.validate(wrongEmailUser));
        assertFalse(userDAO.validate(wrongPasswordUser));

        userDAO.close();
    }
}