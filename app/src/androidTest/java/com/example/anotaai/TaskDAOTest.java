package com.example.anotaai;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import android.content.Context;

import com.example.anotaai.dao.TaskDAO;
import com.example.anotaai.dao.UserDAO;
import com.example.anotaai.database.DatabaseContract;
import com.example.anotaai.models.Task;
import com.example.anotaai.models.User;

import java.util.ArrayList;
import java.util.Date;

@RunWith(AndroidJUnit4.class)
public class TaskDAOTest {
    @Test
    public void insert() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());

        taskDAO.insert(task);

        assertTrue(task.getId() > 0);

        taskDAO.close();
        userDAO.close();
    }

    @Test
    public void get() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());

        taskDAO.insert(task);

        Task result = taskDAO.get(task.getId());

        assertNotNull(result);
        assertSame(result.getId(), task.getId());
        assertSame(result.getUserId(), task.getUserId());
        assertEquals(result.getTitle(), task.getTitle());
        assertEquals(result.getDescription(), task.getDescription());
        assertSame(result.isDone(), task.isDone());
        assertEquals(result.getDue(), task.getDue());

        taskDAO.close();
        userDAO.close();
    }

    @Test
    public void getAll() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());
        Task otherTask = new Task(user.getId(), "Do Something Else", "Have to do something important again", true, new Date());

        taskDAO.insert(task);
        taskDAO.insert(otherTask);

        ArrayList<Task> tasks = taskDAO.getAll();

        assertEquals(tasks.size(), 2);

        taskDAO.close();
        userDAO.close();
    }

    @Test
    public void getAllWithUserId() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());
        Task otherTask = new Task(user.getId(), "Do Something Else", "Have to do something important again", true, new Date());

        taskDAO.insert(task);
        taskDAO.insert(otherTask);

        ArrayList<Task> tasks = taskDAO.getAll(user.getId());

        assertEquals(tasks.size(), 2);

        taskDAO.close();
        userDAO.close();
    }

    @Test
    public void update() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());

        taskDAO.insert(task);

        task.setTitle("Do Something Else");
        task.setDescription("Have to do something important again");
        task.setDone(false);
        task.setDue(new Date());

        taskDAO.update(task.getId(), task);

        Task result = taskDAO.get(task.getId());

        assertNotNull(result);
        assertSame(result.getId(), task.getId());
        assertSame(result.getUserId(), task.getUserId());
        assertEquals(result.getTitle(), task.getTitle());
        assertEquals(result.getDescription(), task.getDescription());
        assertSame(result.isDone(), task.isDone());
        assertEquals(result.getDue(), task.getDue());

        taskDAO.close();
        userDAO.close();
    }

    @Test
    public void delete() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        appContext.deleteDatabase(DatabaseContract.DATABASE_NAME);

        UserDAO userDAO = new UserDAO(appContext);
        userDAO.open();

        User user = new User("jhon.doe@domain.com", "secret");

        userDAO.insert(user);

        TaskDAO taskDAO = new TaskDAO(appContext);
        taskDAO.open();

        Task task = new Task(user.getId(), "Do Something", "Have to do something important", true, new Date());

        taskDAO.insert(task);

        assertEquals(taskDAO.delete(task.getId()), 1);

        taskDAO.close();
        userDAO.close();
    }
}