package com.example.anotaai;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import com.example.anotaai.database.DatabaseHelper;

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    @Test
    public void databaseConnection() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        DatabaseHelper databaseHelper = new DatabaseHelper(appContext);

        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        assertTrue(database.isOpen());

        database.close();
        databaseHelper.close();

        assertFalse(database.isOpen());
    }
}