from flask import Flask, Blueprint, jsonify, request

from osyris.extensions.database import db
from osyris.models import User

auth = Blueprint("auth", __name__, url_prefix="/auth")


@auth.route("/login", methods=["POST"])
def login():
    request_data = request.get_json()

    if "email" not in request_data:
        res = {"data": None, "message": "email is required", "status": 400}
        return jsonify(res), 400

    if "password" not in request_data:
        res = {"data": None, "message": "password is required", "status": 400}
        return jsonify(res), 400

    email = request_data["email"]
    password = request_data["password"]

    user = User.query.filter_by(email=email, password=password).first()

    if user == None:
        res = {"data": None, "message": "invalid email or password", "status": 409}
        return jsonify(res), 409

    res = {"data": user, "message": "logged in", "status": 200}

    return jsonify(res), 200


def init_app(app: Flask) -> None:
    app.register_blueprint(auth)


def init_bp(bp: Blueprint) -> None:
    bp.register_blueprint(auth)
