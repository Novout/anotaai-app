from flask import Flask, Blueprint, jsonify, request
from datetime import datetime

tasks = Blueprint("tasks", __name__, url_prefix="/tasks")

from sqlalchemy.exc import IntegrityError

from osyris.extensions.database import db
from osyris.models import Task


@tasks.route("", methods=["GET"])
def index():
    user_id = request.args.get("user_id", default=0, type=int)

    if user_id != 0:
        tasks = Task.query.filter_by(user_id=user_id).all()
    else:
        tasks = Task.query.all()

    res = {"data": tasks, "message": "index", "status": 200}

    return jsonify(res), 200


@tasks.route("/<int:id>", methods=["GET"])
def get(id: int):
    task = Task.query.get(id)

    if task == None:
        res = {"data": task, "message": "task not found", "status": 404}
        return jsonify(res), 404

    res = {"data": task, "message": "task found", "status": 200}

    return jsonify(res), 200


@tasks.route("", methods=["POST"])
def post():
    request_data = request.get_json()

    if "user_id" not in request_data:
        res = {"data": None, "message": "user_id is required", "status": 400}
        return jsonify(res), 400

    if "title" not in request_data:
        res = {"data": None, "message": "title is required", "status": 400}
        return jsonify(res), 400

    if "description" not in request_data:
        res = {"data": None, "message": "description is required", "status": 400}
        return jsonify(res), 400

    if "done" not in request_data:
        res = {"data": None, "message": "done is required", "status": 400}
        return jsonify(res), 400

    if "due" not in request_data:
        res = {"data": None, "message": "due is required", "status": 400}
        return jsonify(res), 400

    user_id = request_data["user_id"]
    title = request_data["title"]
    description = request_data["description"]

    done = False

    if request_data["done"] == 1:
        done = True

    due = datetime.fromisoformat(request_data["due"])

    task = Task(
        user_id=user_id, title=title, description=description, done=done, due=due
    )

    try:
        db.session.add(task)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        res = {"data": None, "message": None, "status": 409}
        return jsonify(res), 409

    res = {"data": task, "message": "created", "status": 201}

    return jsonify(res), 201


# @tasks.route("/<int:id>", methods=["PUT"])
# def put(id: int):
#     return 200


@tasks.route("/<int:id>", methods=["PUT"])
def put(id: int):
    request_data = request.get_json()

    if "user_id" not in request_data:
        res = {"data": None, "message": "user_id is required", "status": 400}
        return jsonify(res), 400

    if "title" not in request_data:
        res = {"data": None, "message": "title is required", "status": 400}
        return jsonify(res), 400

    if "description" not in request_data:
        res = {"data": None, "message": "description is required", "status": 400}
        return jsonify(res), 400

    if "done" not in request_data:
        res = {"data": None, "message": "done is required", "status": 400}
        return jsonify(res), 400

    if "due" not in request_data:
        res = {"data": None, "message": "due is required", "status": 400}
        return jsonify(res), 400

    user_id = request_data["user_id"]
    title = request_data["title"]
    description = request_data["description"]

    done = False

    if request_data["done"] == 1:
        done = True

    due = datetime.fromisoformat(request_data["due"])

    task = Task.query.get(id)

    if task == None:
        res = {"data": task, "message": "task not found", "status": 404}
        return jsonify(res), 404

    task.user_id = user_id
    task.title = title
    task.description = description
    task.done = done
    task.due = due

    db.session.commit()

    res = {"data": task, "message": "updated", "status": 200}

    return jsonify(res), 200


@tasks.route("/<int:id>", methods=["DELETE"])
def delete(id: int):
    task = Task.query.get(id)

    if task == None:
        res = {"data": task, "message": "task not found", "status": 404}
        return jsonify(res), 404

    db.session.delete(task)
    db.session.commit()

    res = {"data": None, "message": "deleted", "status": 200}

    return jsonify(res), 200


def init_app(app: Flask) -> None:
    app.register_blueprint(tasks)


def init_bp(bp: Blueprint) -> None:
    bp.register_blueprint(tasks)
