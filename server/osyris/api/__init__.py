from flask import Flask, Blueprint

api = Blueprint("api", __name__, url_prefix="/api")

from . import auth
from . import users
from . import tasks

def init_app(app: Flask) -> None:
    auth.init_bp(api)
    users.init_bp(api)
    tasks.init_bp(api)

    app.register_blueprint(api)
