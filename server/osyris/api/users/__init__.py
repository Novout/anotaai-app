from flask import Flask, Blueprint, jsonify, request

from sqlalchemy.exc import IntegrityError

from osyris.extensions.database import db
from osyris.models import User

users = Blueprint("users", __name__, url_prefix="/users")


@users.route("", methods=["GET"])
def index():
    users = User.query.all()

    res = {"data": users, "message": "index", "status": 200}

    return jsonify(res), 200


@users.route("/<int:id>", methods=["GET"])
def get(id: int):
    user = User.query.get(id)

    if user == None:
        res = {"data": user, "message": "user not found", "status": 404}
        return jsonify(res), 404

    res = {"data": user, "message": "user found", "status": 200}

    return jsonify(res), 200


@users.route("", methods=["POST"])
def post():
    request_data = request.get_json()

    if "email" not in request_data:
        res = {"data": None, "message": "email is required", "status": 400}
        return jsonify(res), 400

    if "password" not in request_data:
        res = {"data": None, "message": "password is required", "status": 400}
        return jsonify(res), 400

    email = request_data["email"]
    password = request_data["password"]

    user = User(email=email, password=password)

    try:
        db.session.add(user)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        res = {"data": None, "message": "email already registered", "status": 409}
        return jsonify(res), 409

    res = {"data": user, "message": "created", "status": 201}

    return jsonify(res), 201


@users.route("/<int:id>", methods=["PUT"])
def put(id: int):
    request_data = request.get_json()

    if "email" not in request_data:
        res = {"data": None, "message": "email is required", "status": 400}
        return jsonify(res), 400

    if "password" not in request_data:
        res = {"data": None, "message": "password is required", "status": 400}
        return jsonify(res), 400

    email = request_data["email"]
    password = request_data["password"]

    user = User.query.get(id)

    if user == None:
        res = {"data": user, "message": "user not found", "status": 404}
        return jsonify(res), 404

    user.email = email
    user.password = password

    db.session.commit()

    res = {"data": user, "message": "updated", "status": 200}

    return jsonify(res), 200


@users.route("/<int:id>", methods=["DELETE"])
def delete(id: int):
    user = User.query.get(id)

    if user == None:
        res = {"data": user, "message": "user not found", "status": 404}
        return jsonify(res), 404

    db.session.delete(user)
    db.session.commit()

    res = {"data": None, "message": "deleted", "status": 200}

    return jsonify(res), 200


def init_app(app: Flask) -> None:
    app.register_blueprint(users)


def init_bp(bp: Blueprint) -> None:
    bp.register_blueprint(users)
