from flask import Flask

from osyris import config
from osyris import extensions
from osyris import api


def create_app():
    app = Flask(__name__)

    config.init_app(app)
    extensions.database.init_app(app)
    api.init_app(app)

    return app
