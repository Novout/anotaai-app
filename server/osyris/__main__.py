from osyris import app

def main():
    application = app.create_app()

    application.run()

if __name__ == "__main__":
    main()
