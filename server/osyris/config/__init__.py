import os
from flask import Flask


BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
DATABASE_DIR = os.path.join(BASE_DIR, "database")


class Config(object):
    SECRET_KEY = "eHn9B3WI4XRz2Be3lRzszhuvRfJ7ULGI7fbUsibX7T-ehst0B-Xn-9IzraGeDkbGnPNbTXY2DuB4jX4vUuiCDg"


class Production(Config):
    ENV = "production"
    DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(
        DATABASE_DIR, "production.db"
    )


class Development(Config):
    ENV = "development"
    DEBUG = True

    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(
        DATABASE_DIR, "development.db"
    )


class Testing(Config):
    ENV = "development"
    TESTING = True

    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(DATABASE_DIR, "testing.db")


def init_app(app: Flask) -> None:
    config = Development()

    app.config.from_object(config)
