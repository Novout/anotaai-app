from dataclasses import dataclass

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from osyris.extensions.database import db

from .task import Task


@dataclass
class User(db.Model):
    __tablename__ = "users"

    id: int
    email: str
    password: str
    # tasks: Task

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False)
    # tasks = db.relationship(Task)
