from dataclasses import dataclass
import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from osyris.extensions.database import db


@dataclass
class Task(db.Model):
    __tablename__ = "tasks"

    id: int
    user_id: int
    title: str
    description: str
    done: bool
    due: datetime.datetime

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    title = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(256), nullable=False)
    done = db.Column(db.Boolean, default=False, nullable=False)
    due = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
