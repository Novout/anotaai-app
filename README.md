# AnotaAi!

AnotaAi! é um aplicativo criado por Giovane.C e Thiago.R com o intuito de oferecer maior organização para o usuário comum.

### Telas

![Main](./images/main.png)
![Main](./images/success.png)
![Main](./images/dashboard.png)
![Main](./images/dashboard_create.png)
![Main](./images/dashboard_create_timer.png)

### Banco de Dados

![Database](./images/database.png)

### Features Atuais (7/5)

- Transição de tela (Main - SuccesEnter)
- Transição de tela (SuccesEnter - Dashboard)
- Transição de tela (Dashboard - CreateTask)
- Transição de tela (CreateTask - Dashboard)
- Criação de Conta + Login Imediato
- Validar se a conta já existe
- Inserção de Task pela Conta Atual

### Servidor

O servidor foi desenvolvido utilizando a linguagem [Python][python-url] juntamente com a framework [Flask][flask-url].

#### Features do Servidor

- Usuário [`CRUD`]
- Tarefa [`CRUD`]
- Autenticação [`BASIC`]

#### Utilização

 - Requisítos
   - [Python][python-url] [^3.9]
   - [Poetry][poetry-url] [^1.1]

> Navegue para o diretório do servidor
```sh
    cd server
```

> Instale as dependências utilizando o [Poetry][poetry-url]
```sh
    poetry install
```

> Inicialize a aplicação utilizando o [Poetry][poetry-url]
```sh
    poetry run osyris
```
> O *endpoint* do servidor será: `http://localhost:5000/api`

#### Rotas

```
Rota                    Método 
---------------         -------
/api/auth/login         POST   

/api/users              GET    
/api/users/<int:id>     GET    
/api/users              POST   
/api/users/<int:id>     PUT    
/api/users/<int:id>     DELETE 

/api/tasks              GET    
/api/tasks/<int:id>     GET    
/api/tasks              POST   
/api/tasks/<int:id>     PUT    
/api/tasks/<int:id>     DELETE 
```

#### Exemplos de Requisições

- Criação de Usuário [`POST` `/api/users`]
> Requisição
```json
{
    "email": "jhon.doe@domain.com",
    "password": "secret"
}
```
> Resposta
```json
{
    "data": {
        "email": "jhon.doe@domain.com",
        "id": 1,
        "password": "secret"
    },
    "message": "created",
    "status": 201
}
```

- Autenticação de Usuário [`POST` `/api/auth/login`]
> Requisição
```json
{
    "email": "jhon.doe@domain.com",
    "password": "secret"
}
```
> Resposta
```json
{
    "data": {
        "email": "jhon.doe@domain.com",
        "id": 1,
        "password": "secret"
    },
    "message": "logged in",
    "status": 200
}
```

- Criação de Tarefas [`POST` `/api/tasks`]
> Requisição
```json
{
    "user_id": 1,
    "title": "Task #1",
    "description": "Have to do something",
    "done": 1,
    "due": "2022-01-17"
}
```
> Resposta
```json
{
    "data": {
        "description": "Have to do something",
        "done": true,
        "due": "2022-01-17",
        "id": 1,
        "title": "Task #1",
        "user_id": 1
    },
    "message": "created",
    "status": 201
}
```

[python-url]: https://www.python.org/
[poetry-url]: https://python-poetry.org/
[flask-url]: https://flask.palletsprojects.com/